import { Component, OnInit } from '@angular/core';
import { RealtyService } from '../realty.service';
import { IRealty } from '../../models/IRealty';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.css']
})
export class ListPageComponent implements OnInit {
  public realties: IRealty[] = [];

  constructor(private realtyService: RealtyService) { }

  ngOnInit() {
    this.getRealties();
  }

  getRealties() {
    this.realtyService.realties.subscribe(realties => {
      realties.map(realty => realty.price /= 10 ** 18);
      this.realties = realties;
    });
  }
}
