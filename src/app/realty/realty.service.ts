import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IRealty } from '../models/IRealty';

@Injectable({
  providedIn: 'root'
})
export class RealtyService {
  private url = '/realties';

  constructor(private http: HttpClient) {}

  get realties(): Observable<IRealty[]> {
    return this.http.get<IRealty[]>(`${environment.api.baseUrl + this.url}`);
  }
}
