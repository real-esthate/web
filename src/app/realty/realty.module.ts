import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RealtyRoutingModule } from './realty-routing.module';
import { ListPageComponent } from './list-page/list-page.component';
import { DetailsPageComponent } from './details-page/details-page.component';
import { CreatePageComponent } from './create-page/create-page.component';
import { MatButtonModule, MatCardModule, MatGridListModule } from '@angular/material';

@NgModule({
  declarations: [ListPageComponent, DetailsPageComponent, CreatePageComponent],
  imports: [
    CommonModule,
    RealtyRoutingModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule
  ]
})
export class RealtyModule { }
