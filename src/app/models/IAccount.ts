import { IRealty } from './IRealty';

export interface IAccount {
  address: string;
  balance: number;
  realties: IRealty[];
}
