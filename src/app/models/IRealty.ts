export interface IRealty {
  price: number;
  postalAddress: string;
  name: string;
  imageUrl: string;
  description: string;
  isSold?: boolean;
  owner?: string;
}
